The output of galileo (in debug mode ``--debug``) is pretty verbose as it outputs all the exchanged bytes between the host computer and the dongle.

Here an explanation of that format.

As explained in the documentation for the `USB protocol <Communicationprotocol>`_, there are two class of messages:

- Control messages (for the dongle)
- Data messages (fort the tracker)

Both class of message has its own format, and is displayed differently in the log.

It is always assumed that the host computer is on the left. So that an arrow pointing left indicate message going to the computer (read by galileo), and arrow pointing to the right indicates message going out of the computer (written by galileo).

The actual length of the message is always part of the payload: the payload is bigger, and only that amount of bytes are relevant. The actual length of the message is indicated at the end of the line after a ``-`` in decimal form.

A simple arrow ``-->``, ``<--`` indicate a control message. Follow the message tag, and the data between parenthesis (``(`` and ``)``) if any. Those are represented in hexadecimal form. When a status message is received, as those are human-readable, they are represented as string.

A double arrow ``==>``, ``<==`` indicate a data message. The data itself is printed between square brackets (``[`` and ``]``). The data is in hexadecimal representation.