Checklist for when performing a release:

1. Verify that it works (test a synchronization with tip, run the tests, Check flake8)
2. Put the correct version number where needed: ``galileo/__init__.py``, the setup script will check that is is consistent. (``setup.py checkversion``)
3. Make sure the CHANGES contains everything, that the top entry is the current release, and the date is set.
4. Check if the README needs rewording.
5. Commit the changes
6. Tag the change (sign the tag ?)
7. Push to bitbucket
8. In another directory (for clean environment): make the release for Pypi (``setup.py sdist upload``) (sign the stuff ?)
9. Add the released version to the bug tracker.
10. Prepare for next version: increase version in README.txt, and galileo/__init__.py
11. Commit again
12. Write a mail to the `mailing list <Mailing list>`_ about the release with the content of the CHANGES, and some pointers to where to find it, and how to contact us.
13. Promote the release on other channels ? Which ? G+, Facebook, Twitter, forums, blog ... ? Just link the freelists page ? Duplicate the content ?
14. Celebrate !