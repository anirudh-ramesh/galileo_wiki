This page describe how to configure the software from Fitbit to generate logs.

Newer Fitbit software version
=============================

From a mail_

.. _mail: http://www.freelists.org/post/galileo/Restore-unencrypted-logs

  Hi,
 
  I don't know if this is well-known, but at least on Mac, one can make
  galileod write unencrypted logs by adding::
  
      <encryptLogs value="false" />
  
  to ``/Library/Preferences/galileo_config.xml``
  
  At which point all logs will be saved in cleartext in
  ``/var/log/com.fitbit.galileo.logs.``
  
  The same thing works on Windows but the file to edit is
  ``C:\ProgramData\FitbitConnect\fitbit_connect_config.xml``.
  
  Hope this helps someone with the reverse engineering process...
  
  -Brendan

Older Fitbit software version
=============================

(Information on this page taken from the issue46_)

.. _issue46: https://github.com/openyou/libfitbit/issues/46

This method is so far the only way used to make galileo works.

Windows
-------

Edit ``fitbit_connect_config.xml`` in the ``C:\ProgramData\FitbitConnect`` directory to add the following lines::

    <logEnabled value="true" />
    <logTrackerCommandBytes value="true" />

Then it should put every USB IO byte (along with other handy info) in a dated log file that rotates.

MacOS X
-------

Edit ``/Library/LaunchDaemons/com.fitbit.galileod.plist``: Change ``/usr/local/bin/galileod`` to ``/usr/local/bin/galileod -e`` and

1. Kill galileod process in Activity Manager (it will restart itself)

2. Open the log at ``/private/var/run/com.fitbit.galileod.log``