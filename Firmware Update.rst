So, I got lucky and caught a Firmware update on logs. Unfortunately, not the network part. So I have no idea how to get that blob (Or maybe it's bundled in the .exe ?).

It's quite easy actually, we have a write and a read command:

Write Firmware
==============

command
-------

``20 F9 06 00 00200000 0221FB023048FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF``

``20``: Total message length

``F9``: Write firmware

``06``: Length of the Addr

``00``: ???

``00200000``: Address where to write (4 bytes)

``0221FB023048FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF``: Data (24 bytes)

The firmware is written 24 bytes at a time: ``00200000``, ``18200000``, ``30200000``, ... ``E8E70100``.

answer
------

The answer is always ``02FE``

Read Firmware
=============

command
-------

``08 F8 06 00 00200000``

(Same as for write)

response
--------

``20 FC 06 00 00200000 0221FB023048FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF``

Here we can see that what was written in the command above can be read again.

Introduction
============

So I lied a bit, there are more commands involved. On top of the write, we find::

    -> FD - 2
    <- FD (00 08 6F7BAD296ABC 7409 00200000 FFE70300 01) - 21
    -> FA - 2
    <- FE - 2

The ``FD`` is about querying info about the bootloader, the ``FA``, I don't know ...

Conclusion
==========

And at the end, we have::

    -> FB (E238B4351DC74BCA) - 10
    <- FE - 2
    -> FD - 2
    <- FD (00 08 6F7BAD296ABC 7409 00200000 FFE70300 01) - 21
    -> FC - 2
    <- FE - 2

The ``FD`` is about querying info about the bootloader, the ``FC`` is probably about closing what the ``FA`` did. The ``FB``, I really don't know ...