About the response of the Megadump ...

It starts like the dump itself.

It has a fixed length:

====== ===========
 Type   Length
====== ===========
 One    164 bytes
 Zip    96 bytes
====== ===========
