This page documents the various commands exchanged between the dongle/tracker and the host computer.

We can distinguish two kind of command (and answers): the one sent to the dongle, and the one sent to the tracker.

The dongle commands are sent on the control channel, and the tracker one on the data channel.

.. note:: The In the following tables, the "Header" or "Type" is in hexadecimal form.

Commands for the dongle
-----------------------

They are always be encoded on 32 bytes. Where the two first one indicate the meaning and the rest the payload. The payload doesn't have to use 30 bytes, the value of the unused one is not relevant.

The first byte indicate the total length of the actual command, and the second byte, the type of the command. The rest is command dependent.

Commands
~~~~~~~~

====== ========== =================
 Type   Payload    Meaning
====== ========== =================
 01     0 bytes    Get dongle information
 02     0 bytes    Disconnect
 04     24 bytes   `Start Discovery <Discover command>`_
 05     0 bytes    Stop Discovery
 06     9 bytes    `Establish airLink <AirLink>`_
 07     0 bytes    `Terminate airLink <AirLink>`_
 08     1 byte     Enable/Disable TX Pipe (payload of 1: enable, payload of 0: disable)
 0D     1 byte     Set Power level
 12     15 bytes   EstablishLinkEx_
 18     2 bytes    ??? Before upload to ask for status ? (`Fast Airlink <FastAirlink>`_)
 19     2 bytes    ??? (Before EstablishLinkEx_)
 F8     6 bytes    Read Firmware ?
 F9     30 bytes   Write firmware ?
 FA     0 bytes    Before Firmware update.
 FB     8 bytes    After firmware update
 FC     0 bytes    After firmware update (contrary of FA ?)
 FD     0 bytes    Query `BootLoader <bootloader>`_ info ?
====== ========== =================

Responses
~~~~~~~~~

====== ========== =================
 Type   Payload    Meaning
====== ========== =================
 01     30 bytes   Status message: payload is a string ('\\0' terminated)
 02     1 byte     Amount of tracker discovered (coded in payload)
 03     17 bytes   Tracker discovered (See `Discover <Discover command>`_)
 04     1 byte     `Link established <AirLink>`_
 05     1 byte     Link terminated (payload is 0x16 or 8)
 06     6 bytes    `link parameters echo <AirLink>`_
 07     0 bytes    `GAP link established <AirLink>`_
 08     19 bytes   Dongle information
 0A     0 bytes    (Some kind of error ?)
 12     1 byte     Status during upload (`Fast Airlink <FastAirlink>`_)
 FC     30 bytes   Answer to Read-Firmware
 FD     19 bytes   `BootLoader <bootloader>`_ info ?
 FE     1 byte     Power level set / ack firmware update (0 bytes payload)
 FF     2 bytes    Error: Command not understood (See #236)
====== ========== =================

Command for the tracker
-----------------------

Those commands are also always 32 bytes, although they follow another encoding scheme: The last bytes (#31) indicate the length of the actual command (which doesn't seems to be ever bigger than 20).

The first byte indicates if the bytes represent a command or raw data. If set to ``C0``, the rest of the bytes represent a command. Else all the bytes are part of a raw data message. To be able to represent the byte ``C0`` in the first position of a raw data message, the first byte of a data message is always SLIP encoded: ``C0`` becomes ``DB DC`` and ``DB`` becomes ``DB DD`` (only when placed in the first position).

In case of commands, the second bytes indicate the nature of the command.

Commands
~~~~~~~~

======== ========== =========
 Header   Length     Meaning
======== ========== =========
 C0 01    2 bytes    reset Link
 C0 02    2 bytes    ack (commit)
 C0 06    2 bytes    Display code
 C0 09    12 bytes   echo (all data bytes set to 0)
 C0 0A    12 bytes   `Initialize airLink <AirLink>`_
 C0 10    3 bytes    Get dump (byte 3 is the kind of the dump)
 C0 23    6 bytes    update secret (?)
 C0 24    9 bytes    `Start of transmission <Transmission>`_
 C0 50    10 bytes   request authentication challenge from tracker
 C0 52    10 bytes   respond to authentication challenge
======== ========== =========

Responses
~~~~~~~~~

======== ========== =========
 Header   Length     Meaning
======== ========== =========
 C0 01    2 bytes
 C0 02    2 bytes
 C0 03    4 bytes    (Some kind of Error when uploading some data ? ``09 10`` already met)
 C0 0B    2 bytes    Response to TX Pipe toggle
 C0 12    5 bytes    ACK First block of data
 C0 13    5 bytes    ACK successive block of data
 C0 14    12 bytes   `airLink established <AirLink>`_
 C0 41    3 bytes    start of dump (byte 3 is the kind of the dump)
 C0 42    9 bytes    end of dump
 C0 51
======== ========== =========