This tries to documents the composition of the trackers.

Looks like all trackers are using the `STM32L151C6 <http://www.st.com/web/catalog/mmc/FM141/SC1544/SS1374/LN1041/PF252048>`_. This chip has a LCD driver, but no AES capabilities, which could be a good sign for us.

Zip
---

-

One
---

- http://www.ifixit.com/Teardown/Fitbit+One+Teardown/19889
- http://conoroneill.net/what-does-this-inside-of-a-dead-fitbit-one-look-like-this/


Flex
----

- http://www.ifixit.com/Teardown/Fitbit+Flex+Teardown/16050
- http://electronics360.globalspec.com/article/3128/teardown-fitbit-flex


Force
-----

- http://learn.adafruit.com/fitbit-force-teardown/inside-fitbit-force
