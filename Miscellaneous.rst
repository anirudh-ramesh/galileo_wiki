Here are links to other pages not about technical details.

* The `mailing list <Mailing list>`_.
* The `release procedure <Release procedure>`_.
* `Generate logs <galileo Logs>`_ from the original software.
* Format of the `generated logs <Log format>`_.