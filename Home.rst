Galileo Wiki
============

Welcome to the galileo wiki. This place is meant as a platform to exchange information about the `communication protocol <Communication protocol>`_ between the host and the fitbit tracker, the `format of the megadumps <Megadump format>`_ extracted from the trackers, the `network communication <Network communication>`_ between the client and the server, and `other bits <Miscellaneous>`_.

This is a community wiki, and as such, it is not (much) organized, and the information over here should not be taken too seriously. It acts as a draft for the documentation. Once the validity of the information present here is verified, it should be transfered to the main documentation.

Go ahead, create pages,modify existing ones (even this one) and get your knowledge down. Don't be afraid of making mistakes, the only one who's wrong is the one who doesn't try.

You are always welcome to head over to the `mailing list <Mailinglist>`_ to discuss a point you are not sure about. Best is to first write it down and then start a discussion, as it is way easier to discuss about written down stuff than vague ideas.

  Please choose ``reStructuredText`` as markup language as it makes later integration into a sphinx document way easier.
