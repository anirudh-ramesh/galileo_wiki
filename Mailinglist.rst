The mailing list is hosted by freelists.org.

Information about the list can be found here: http://freelists.org/list/galileo

The list archive can be found, and searched, here: http://freelists.org/archive/galileo

To subscribe, just send a mail to galileo-request@freelists.org with "subscribe" in the subject.

----

Oh, and if someone ever ask why the Reply-To is not set to the list: http://www.unicom.com/pw/reply-to-harmful.html