This commands instruct the dongle to return the list of trackers in the vicinity.

This command is sent on the control channel, and the answers are also on the control channel.

--> 1A 04 BA 56 89 A6 FA BF A2 BD 01 46 7D 6E 00 00 AB AD 00 FB 01 FB 02 FB A0 0F

There is an UUID hidden in there, as well as three serviceUUID. The UUID is coded on 16 bytes, and the service UUIDs on two bytes. They are all in LSB notation:

+-------+------+--------------+--------------+--------------+
| 14 04 | uuid | serviceUUID1 | serviceUUID2 | serviceUUID3 |
+-------+------+--------------+--------------+--------------+

The meaning of the first serviceUUID is unknown. the second is about a write channel, and the third about the read channel.

Upon successful reception, the dongle will answer

<-- 20 01 StartDiscovery

Followed by the tracker that are present. (It is possible that a tracker get listed twice see issue #14).

Each tracker has its own answer, for instance:

<-- 13 03 tt tt tt tt tt tt TT rr 02 aa aa 03 2C 31 F6 ss ss 00 (13 times)

Where:

- tt are the tracker id (6 bytes)
- TT is the address type
- rr is the RSSI (encoded as a signed char, go figure, I have yet to see a positive one !)
- aa are the 'attributes' (2 bytes) (whatever that means)

  - the second byte has the value 4 when the tracker was recently synchronized, else, the value 6.

- ss are the serviceUUID (2 bytes).

tt, TT, and ss are used when trying to establish a connection with a particular tracker.

To conclude, the amount of discovered tracker is sent like that:

<-- 03 02 nn 00 (29 times)

Where nn is the amount of trackers.