This page is about the communication between the Fitbit server and the Synchronisation client (galileo in our case).

Format of the messages is XML.

The transport protocol is HTTP(S), the ``Content-Type`` header has to be set to ``text/xml`` in order for the server to accept it.

Requests
========

The structure of the messages is as follow:

.. sourcecode:: yaml

    galileo-client:
        version: 2.0
        client-info:
            client-id:
                UUID
            client-version:
                version-string
            client-mode:
                MODE
            dongle-version:
                major: 1
                minor: 1
        ui-response:
            action: ACTION
            param:
                name: NAME
                VALUE
        server-state:
            previous state from server
        command-response:
            COMMANDRESPONSE
        tracker:
            tracker-id: TRACKERID
            data:
                base64 encoded data

MODE can be one of:

- status
- sync
- pair
- firmware

``COMMANDRESPONSE`` is one of:

.. sourcecode:: yaml

    - list-trackers:
        available-tracker:
            tracker-id:
                TRACKERID
            tracker-attributes:
                ATTRIBUTES
            rsi:
                RSSI
    - ack-tracker-data:
        tracker-id: TRACKERID

Responses
=========

The answers from the server are structured as follow:

.. sourcecode:: yaml

    galileo-server:
        version: 2.0
        server-version:
            '\n\n'
        redirect:
            protocol:
                https
            host:
                client.fitbit.com
            port:
                443
        server-state:
            state to be recalled on next communication
        ui-request:
            action: ACTION
                client-display:
                    containsForm: BOOL
                    minDisplayTimeMs: INT
                    width: INT
                    height: INT
                    url: URL
                    URLENCODED HTML PAGE
        commands:
            COMMANDS
        tracker:
            tracker-id: TRACKERID
            type: TYPE
            data:
                base64 encoded dada

Most part are optional, and only there in a few cases. The ``version`` and ``server-version`` is always there.

``ACTION`` can be one of:

- login
- profile
- chooseDeviceType
- deviceList
- removeTrackers
- requestSecret
- pairProgress
- done
- connecting
- syncProgress

``TYPE`` can be one of:

- megadumpresponse
- microdumpresponse

The possible ``COMMANDS`` are:

.. sourcecode:: yaml

    - pair-to-tracker:
        displayCode: BOOL
        waitForUserInput: BOOL
        tracker-id: TRACKERID
    - connect-to-tracker:
        tracker-id: TRACKERID
        response-data: RESPONSEDATA
    - connect-to-tracker:
        tracker-id: TRACKERID
        connection: disconnect
    - list-trackers:
        immediateRsi: -50
        minDuration: 4000
        maxDuration: 13000
        searchAttribute:
            0
    - ack-tracker-data:
        tracker-id: TRACKERID


``RESPONSEDATA`` can be one of:

- megadump
- microdump

