In order to send commands to the tracker, a *airLink* has to be established.

The data commands will be sent over that airLink. The link is a BTLE link between the tracker and the dongle.

See also `Fast Airlink <FastAirlink>`_.

Commands
========

EstablishAirLink
----------------

This is a Control command with INS ``06``, and 9 bytes of payload. The first six are the tracker ID, the next one is the addressType, and the two last one are the serviceUUID of the Tracker.

The dongle will answer with a statusMessage 'EstablishLink' (or 'EstablishLink called' with newer dongles), then ``04 (00) - 3``, then another status Message: 'GAP_LINK_ESTABLISHED_EVENT', and finally ``07 - 2``, which is the link establishment confirmation. 

Example::

    --> 06 (tt tt tt tt tt tt aa ss ss) - 11
    <-- EstablishLink
    <-- 04 ( 00 ) - 3
    <-- GAP_LINK_ESTABLISHED_EVENT
    <-- 07 - 2

ToggleTxPipe
------------

Before we are able to receive anything on the link, we need to enable the txPipe::

    --> 08 ( 01 ) - 3
    <== [ C0 0B ] - 2

Initialise airLink
------------------

At this point, we established a link, and enabled the tx pipe, but still, we didn't set the parameters on the link. This has to be done next::

    ==> [ C0 0A i0 i0 i1 i1 i2 i2 i3 i3 ] - 12
    <-- 06 ( i1 i1 i2 i2 i3 i3 ) - 8
    <== [ C0 14 p1 p1 p2 p2 tt tt tt tt tt tt ] - 12

In case of the Charge tracker, the last line becomes::

    <== [ C0 14 p1 p1 p2 p2 tt tt tt tt tt tt p3 p3 ] - 14

There are a couple of parameters there:

- ``i0``, ``i1``, ``i2`` and ``i3`` are the initialisation parameters. So far, two set are known to work: (10, 6, 6, 0, 200), and (1, 8, 16, 0, 200). Note that ``i1``, ``i2``, and ``i3`` are echoed by the dongle.
- ``p1``, ``p2`` and ``p3`` is another set of parameters. It seems to be tracker type dependant:

   ============== =======
    Tracker type   Value
   ============== =======
    Zip            12, 8
    One            268, 0
    Flex           268, 2
    Charge        2572, 0, 23
   ============== =======