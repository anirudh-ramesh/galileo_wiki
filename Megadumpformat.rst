This page documents the format of the megadump extracted from the trackers.

There are two known kind of formats for the dumps:

- the plain format
- the encrypted format

The Flex and the Force are known to produce the encrypted format, the One, after a firmware update will also switch to the encrypted format.

So what is the *encrypted format* ? It could be AES, if the `chip on board <Electronics>`_ understand this, and if all dumps are of length multiple of 128 bytes (AES128).

The first byte is an identifier for the type of the tracker used:

======= =========
 Value   Meaning
======= =========
 0x26    One
 0x28    Flex
 0xF4    Zip
 0x2E    Charge HR
======= =========

Then follows ``0x01`` or ``0x02``.

Both the dump and its `response <Megadump response>`_ follow this format.

All dumps have a header and a footer. The footer is composed of the last 9 (?) bytes. It contains the length of the dump itself minus the length of the footer.