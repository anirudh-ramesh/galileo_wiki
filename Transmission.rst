An example of the header of a transmission packet is::


    C02404C5 00000053 E4


Here ``C0 24`` indicates that it is the start of a transmission. The value ``C5`` equals 197 and is the length of the payload.
