Here: From the logs::


    03/06 14:27:59 [INFO] *** CONTROL OUT bytes: 02FD
    03/06 14:27:59 [INFO] *** CONTROL IN bytes: 15FD00086F7BAD296ABC740900200000FFE70300010000000000000000000000
    03/06 14:27:59 [INFO] CLIENT: [03/06 14:27:59] Dongle | Bootloader Version | {"majorVersion":0,"minorVersion":8,"deviceAddr":[111,123,173,41,106,188],"flashEraseTime":2420,"firmwareStartAddress":8192,"firmwareEndAddress":255999,"ccIC":1}


So I'd say the info is:

major-minor-deviceAddr(6)-flashEraseTime(2)-startAddr(4)-endAddr(4)-ccIC

``00-08-6F7BAD296ABC-7409-00020000-FFE70300-01``