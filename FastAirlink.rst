There also exist a Fast `Airlink <AirLink>`_:

During upload, the data is not validated over the data channel, but over the control channel.

Example::

    => [ F8 24 0D 90 F8 FA 00 03 28 05 D0 09 28 04 D0 0A 28 01 D0 0D ] - 20
    <- 12 (00) - 3

When it fails::

    => [ 24DFF8AC0C90F8FB00002800F07F81012800F0B2 ] - 20
    <- 12 (04) - 3

This is called a "Queue failure", with status 4 in the logs. Solution : just try again (after 49 tries, it might work).

Start of transmission for a microdump, with the first block sent::
    
    => [ C0 24 01 49 98 00 00 34 99 00 ] - 10
    <= [ C0 12 01 00 00 00 ] - 6
    -> 18 (01 00) - 4
    => [ 3102000000000100000030980000120100020008 ] - 20
    <- 12 (00) - 3

Same for a megadump::

    => [ C0 24 04 2E 01 00 00 65 A3 00 ] - 10
    <= [ C0 12 04 00 00 00 ] - 6
    -> 18 (01 00) - 4
    => [ 2E 02 00 00 00 00 01 00 00 00 2E 02 00 00 00 00 51 10 00 00 ] - 20
    <- 12 (00) - 3

This is what I believe the end of transmission (with the last block being sent)::

    => [ 00 84 8C 04 ] - 4
    <- 12 (00) - 3
    => [ C0 02 ] - 2
    <- 12 (00) - 3
    <= [ C0 02 ] - 2

When Fast Airlink is not working, we get that (I'm not sure I understand it either)::

    => [ C0 24 04 C5 00 00 00 B7 BA 00 ] - 10
    <= [ C0 12 04 00 00 ] - 5
    Dongle | Fast Airlink | Tracker does not support Fast Airlink. Falling back to Classic Airlink
    -> 19 (01 00) - 4
    